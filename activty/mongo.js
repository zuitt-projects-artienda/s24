db.users.find({ $or: [{"lastName" : {$regex: 'y', $options: '$i'}}, {"firstName" : {$regex: 'y', $options: '$i'}} ]
	}, {"_id" : 0, "email" : 1})


db.users.find({ $and: [{"firstName" : {$regex: 'e', $options: '$i'}}, {"isAdmin" : true} ]
	}, {"_id" : 0, "email" : 1, "isAdmin" : 1})


db.products.find({ $and: [{"name" : {$regex: 'x', $options: '$i'}}, {"price" : {$gte: 50000}}]})

db.products.updateMany({"price" : {$lt: 2000}}, {$set: {"isActive" : false}})

db.products.deleteMany({"price" : {$gt: 20000}})
